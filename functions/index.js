const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();

exports.addUserRole = functions.auth.user().onCreate(async (user) => {
		// const customClaims = {
		// 	admin: false
		// };
		// return await admin.auth().setCustomUserClaims(user.uid, customClaims);
});


exports.createAccount = functions.https.onCall(async (data, context) => {
	try {
		await admin.auth().createUser({
			email: data.email,
			password: data.password,
			displayName: data.name,
		})
		.then(userRecord => {

			let claims = {
				admin: data.admin
			}

			if((data.admin === 'true') || (data.admin === true)) {
				claims.department = 'admin';
			} else {
				claims.department = data.department;
			}

			return admin.auth().setCustomUserClaims(userRecord.uid, claims);
		})
		return await true;
	} catch (error) {
		console.log(error)
		return false;
	}
});











exports.setUserRole = functions.https.onCall(async (data, context) => {

	try {
		// if (context.auth.token.role !== 'admin') {
		// 	let errorMessage =  { code : 403, message : myMessage };
		// 	throw errorMessage;
		// 	throw 'Not admin';
		// }

		await admin.auth().setCustomUserClaims(data.uid, {admin:data.admin});
		return await true;
	} catch (error) {
		console.log(error)
	}

});

exports.listAllUsers = functions.https.onCall(async (data, context) => {
	// if (!context.auth.token.admin) return
	try {
		var allUsers = [];
		await admin.auth().listUsers()
		.then(listUsersResult => {
			listUsersResult.users.map(userRecord =>{
				return allUsers.push({
					id: userRecord.uid,
					email: userRecord.email,
					name: userRecord.displayName,
					department: userRecord.customClaims.department,
					admin: userRecord.customClaims.admin,
					last_sign_in : userRecord.metadata.lastSignInTime,
					created_at: userRecord.metadata.creationTime
				});
			});
			return true;
		}).catch(function(error) {
			console.log('Error listing users:', error);
		});
		return allUsers;
	} catch (error) {
		console.log(error)
	}

});


exports.getAccountInfo = functions.https.onCall(async (data, context) => {
	
	try {
		let user = null;
		await admin.auth().getUser(data.id)
		.then(userRecord => {
			console.log('Successfully fetched user data:', userRecord.toJSON());
			user = {
				id: userRecord.uid,
				email: userRecord.email,
				name: userRecord.displayName,
				department: userRecord.customClaims.department,
				admin: userRecord.customClaims.admin,
			};
			return true;
		})
		.catch(error => {
			console.log('Error fetching user data:', error);
		});
		return user;
	} catch (error) {
		console.log(error)
	}
});



exports.updateAccount = functions.https.onCall(async (data, context) => {
	// if (!context.auth.token.admin) return
	try{

		let updateData = {
			email: data.email,
			displayName: data.name
		};

		if(data.password !== '') {
			updateData.password = data.password;
		} 

			let claims = {
				admin: data.admin,
			}

			if((data.admin === 'true') || (data.admin === true)) {
				claims.department = 'admin';
			} else {
				claims.department = data.department;
			}

		await admin.auth().updateUser(data.id,updateData);
		await admin.auth().setCustomUserClaims(data.id,claims)
		return await true;
	} catch (error) {
		console.log(error)
	}

});


exports.removeAccount = functions.https.onCall(async (data, context) => {
	try {
		let status = false;
		admin.auth().deleteUser(data.id)
		.then(() => {
			return status = true;
		})
		.catch(error => {
			console.log('Error deleting user:', error);
		});
		return status;
	} catch (error) {
		console.log(error)
	}
});

