import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);


const DefaultContainer = () => import ("@/views/templates/DefaultContainer.vue")

const appTitle = 'Document Log & Record System';


export default new Router({
  mode: "history",
  // base: process.env.BASE_URL,
  routes: [{
    path: "/",
    redirect: '/departments',
    component: DefaultContainer,
    children: [
    {
      path: "departments",
      name: "Departments",
      component: () => import ("@/views/pages/departments/Index.vue"),
      meta: {
        title: `departments - ${appTitle}`,
        auth: true
      }
    },
    {
      path: "accounts",
      name: "Accounts",
      component: () => import ("@/views/pages/accounts/Index.vue"),
      meta: {
        title: `Accounts - ${appTitle}`,
        auth: true,
        admin: true,
      }
    },
     {
      path: "logs",
      name: "All Logs",
      component: () => import ("@/views/pages/logs/Index.vue"),
      meta: {
        title: `Logs - ${appTitle}`,
        auth: true,
        admin: true,
      }
    },

    ]

  }, 






  {
    path: '/logs',
    component: DefaultContainer,
    children: [

    {
      path: "department/:id",
      name: "Department Logs",
      component: () => import ("@/views/pages/logs/DepartmentLogs.vue"),
      meta: {
         auth: true,
        title: `Department Logs - Create - ${appTitle}`,
      }
    },

    ]
  },



















  {
    path: '/accounts',
    component: DefaultContainer,
    children: [

    {
      path: "create",
      name: "Create Account",
      component: () => import ("@/views/pages/accounts/Create.vue"),
      meta: {
        admin: true,
         auth: true,
        title: `Accounts - Create - ${appTitle}`,
      }
    },

    {
      path: "edit-account-information/:id",
      name: "Edit Account Information",
      component: () => import ("@/views/pages/accounts/Edit.vue"),
      meta: {
        admin: true,
         auth: true,
        title: `Edit Account Information - ${appTitle}`,
      }
    },

    {
      path: "show-account-information/:id",
      name: "Show Account Information",
      component: () => import ("@/views/pages/accounts/Show.vue"),
      meta: {
        admin: true,
         auth: true,
        title: `Show Account Information - ${appTitle}`,
      }
    },

    ]
  },






















  {
    path: '/departments',
    component: DefaultContainer,
    children: [

    {
      path: "create",
      name: "Create department",
      component: () => import ("@/views/pages/departments/Create.vue"),
      meta: {
         auth: true,
        title: `department - Create - ${appTitle}`,
      }
    },

    {
      path: "add-file/:id",
      name: "Add File",
      component: () => import ("@/views/pages/departments/AddFile.vue"),
      meta: {
         auth: true,
        title: `department - Add File - ${appTitle}`,
      }
    },

    {
      path: "edit-department-information/:id",
      name: "Edit department Information",
      component: () => import ("@/views/pages/departments/Edit.vue"),
      meta: {
         auth: true,
        title: `Edit Department Information - ${appTitle}`,
      }
    },

    {
      path: "show-department-information/:id",
      name: "Show department Information",
      component: () => import ("@/views/pages/departments/Show.vue"),
      meta: {
         auth: true,
        title: `Show department Information - ${appTitle}`,
      }
    },


    {
      path: "view-files/:id",
      name: "View Files",
      component: () => import ("@/views/pages/departments/ViewFiles.vue"),
      meta: {
         auth: true,
        title: `departments - View Files - ${appTitle}`,
      }
    },



    ]
  },















  {
    path: '/',
    component: {
      render (c) { return c('router-view') }
    },
    children: [

     {
      path: "/sm52miox2pdm1xxbjsyhayr931axw74xdnmlo062im8z9u0dzl5fh0200oopmt910wcxv",
      name: "Admin Account Generator",
      component: () =>  import ("@/views/components/AdminAccountGenerator.vue"),
      meta: {
        title: `Admin Account Generator - ${appTitle}`,
      }
    },

    {
      path: "/auth",
      name: "Auth",
      component: () =>  import ("@/modules/auth/Index.vue"),
      meta: {
        title: `Auth - ${appTitle}`,
      }
    }, {
      path: "*",
      redirect: '/departments',
    }
    ],
  }











  ]
});