import Axios from "axios";
import Vue from "vue";
const fb = require('@/firebaseConfig.js')

const loginUser = ({commit, state}, payload) => {
  return new Promise((resolve, reject) => {

    fb.auth
    .signInWithEmailAndPassword(state.email, state.password)
    .then(data => {

      localStorage.setItem('name', data.user.displayName);
      localStorage.setItem('email', data.user.email);

      fb.auth.currentUser.getIdTokenResult(true).then(user=> {
        if(user.claims.admin == true) {
          // this.$router.push({
          //   name: "Departments"
          // });
          // window.location.href = '/departments'

          // this.$router.replace({
          //   path: "departments"
          // });

          vm.$router.push('/departments');
          resolve(true)
        } else {

          var query = fb.db.collection("departments")
          .where("id_number", "==", user.claims.department);
          query.get().then(res => {
            if(res.docs.length > 0) {
              localStorage.setItem('id_number', user.claims.department);
              localStorage.setItem('department', res.docs[0].id);
              localStorage.setItem('department_name', res.docs[0].data().name);

              let upath = `/departments/view-files/${res.docs[0].id}`
              vm.$router.push(upath);
              resolve(true)

            } else {
              alert('User department not exist');
              reject('User department not exist');
            }
          })


        }
      })

      resolve(true);
    })
    .catch(err => {
      reject(err.message);
    });

  });
};

const logoutUser = state => {
  return new Promise((resolve, reject) => {
    localStorage.clear();
    fb.auth
    .signOut()
    vm.$router.push({name: 'Auth'});

    resolve(true)
  })
};

const registerUser = ({commit, state}, payload) => {
  return new Promise((resolve, reject) => {
   fb.auth
   .createUserWithEmailAndPassword(state.email, state.password)
   .then(data => {
    data.user
    .updateProfile({
      displayName: state.name
    })
    .then(() => {
      resolve(true);
    });
  })
   .catch(err => {
     reject(err.message);
   });

 });
};



export default {
  loginUser,
  registerUser,
  logoutUser,
};
