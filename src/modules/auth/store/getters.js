
const getEmail = (state) => {
	return state.email;
}
const getPassword = (state) => {
	return state.password;
}

export default {
	getEmail,
	getPassword,
};
