import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/database'



// firebase init goes here
const config = {
  apiKey: "AIzaSyC9EidXzLghidck8s6FM1jb-QJCK9Gnnww",
  authDomain: "aivan-d15fa.firebaseapp.com",
  databaseURL: "https://aivan-d15fa.firebaseio.com",
  projectId: "aivan-d15fa",
  storageBucket: "aivan-d15fa.appspot.com",
  messagingSenderId: "955029606939",
  appId: "1:955029606939:web:8261163ce6bb54f3509ebe"
};

firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const realtime = firebase.database();
const currentUser = auth.currentUser

// date issue fix according to firebase
// const settings = {
//     timestampsInSnapshots: true
// }
db.settings({})

// firebase collections

export {
  firebase,
  storage,
  db,
  realtime,
  auth,
  currentUser,
}
