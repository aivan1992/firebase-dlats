
import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import Axios from 'axios'
Axios.defaults.baseURL='http://localhost:8000/api'

import store from "./store";

import customPlugin from './custom-plugins';
Vue.use(customPlugin)

import VueGoodTablePlugin from 'vue-good-table'
Vue.use(VueGoodTablePlugin);
import 'vue-good-table/dist/vue-good-table.css'

import '@/externalScript'

/*
*  IMPORT THE GLOBAL MIXIN HELPERS
*/
import {mixin} from '@/mixins/index';
/*
*/

import VueQrcodeReader from "vue-qrcode-reader";
Vue.use(VueQrcodeReader);

import fullscreen from 'vue-fullscreen'
  Vue.use(fullscreen)


import firebase from 'firebase/app'
import 'firebase/auth'

router.beforeEach((to, from, next) => {

  if (to.matched.some(record => record.meta.auth)) {
    firebase.auth().onAuthStateChanged(function(user) {
      if (!user) {
        next({
          path: '/auth'
        })
      } else {
        firebase.auth().currentUser.getIdTokenResult(true).then(res=> {

         if(to.path == '/departments' && !res.claims.admin) {
         return  next({
            path: vm.getStaffRoute()
          });
         }



         if(to.matched.some(record => record.meta.admin)) {
          if(res.claims.admin) {
            next()
          } else {

           next({
            path: vm.getStaffRoute()
          });
         }
       } else {
        next()
      }
    })
      }
    })
  } else {
    next()
  }
});









import Swal from 'sweetalert2'
window.Swal = Swal;



router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));

  next();
});




Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    window.event = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', window.event)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', window.event)
  },
});



Vue.config.productionTip = false;

const fb = require('./firebaseConfig.js')

var app;

fb.auth.onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      mixins: [mixin],
      router,
      store,
      watch: {
        "$route"(to,from) {
          this.$store.commit('global/setBreadcrumbs', {
            redirect_to: from.path,
            route_from: from.name,
            route_to: to.name
          });
        }
      },
      render: h => h(App)
    }).$mount("#app")
  }
  global.vm = app;
})


