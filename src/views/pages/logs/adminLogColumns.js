 const desktop = [
  {
 	field: 'id',
 	label: 'ID',
 },
 {
 	field: 'file_name',
 	label: 'FILE NAME',
 },{
 	field: 'activity',
 	label: 'ACTIVITY',
 }, {
 	field: 'message',
 	label: 'MESSAGE',
 }, {
	field: 'created_at',
	label: 'TRANSACTED AT',

}, 
// {
//  	field: 'receiver',
//  	label: 'RECEIVER ID',
//  },{
//  	field: 'sender',
//  	label: 'SENDER ID',
//  },
 {
 	field: 'actions',
 	label: 'VIEW FILE',
 }];

 const mobile = [{
 	field: 'log_info',
 	label: 'LOG INFO',
 }];

 export default {
 	desktop,
 	mobile
 }