 const desktop = [
{
 	field: 'id',
 	label: 'ID',
 },
 {
 	field: 'file_name',
 	label: 'FILE NAME',
 },{
 	field: 'activity',
 	label: 'ACTIVITY',
 }, {
 	field: 'created_at',
 	label: 'TRANSACTED AT',
 }, {
 	field: 'message',
 	label: 'MESSAGE',
 }, {
 	field: 'receiver',
 	label: 'RECEIVER ID',
 },{
 	field: 'sender',
 	label: 'SENDER ID',
 }, {
 	field: 'actions',
 	label: 'ACTIONS',
 }];

 const mobile = [{
 	field: 'log_info',
 	label: 'LOG INFO',
 }];

 export default {
 	desktop,
 	mobile
 }