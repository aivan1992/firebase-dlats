const desktop =  [
{
	field: 'id_number',
	label: 'ID NUMBER',
}, {
	field: 'name',
	label: 'DEPARTMENT NAME',
}, {
	field: 'actions',
	label: 'ACTIONS',
},
{
	field: 'remove',
	label: 'REMOVE',
}
]


const mobile = [{
	field: 'department_info',
	label: 'DEPARTMENT INFO',
}];



export default {
	desktop,
	mobile
}