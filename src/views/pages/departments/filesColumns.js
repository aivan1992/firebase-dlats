const desktop =  [
{
	field: 'name',
	label: 'FILE NAME',
}, {
	field: 'type',
	label: 'FILE TYPE',
	tdClass: 'capitalize-text',
},{
	field: 'size',
	label: 'SIZE',
	formatFn: value => vm.formatAsThousands(value) + ' kb'
},{
	field: 'created_at',
	label: 'CREATED AT',

	formatFn: value => {
		if(value !== null) {
			let newDate =  new Date(value);
			return moment(newDate).format('MMMM Do YYYY, h:mm:ss a');
		}
		return value;
	}

},{
	field: 'actions',
	label: 'ACTIONS',
}, {
	field: 'remove',
	label: 'REMOVE',
}];


 const mobile = [{
 	field: 'file_info',
 	label: 'FILE INFO',
 }];



export default {
	desktop,
	mobile
}