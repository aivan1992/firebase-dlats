import Axios from "axios";
import Vue from "vue";

const fb = require('@/firebaseConfig.js')
import firebase from "firebase/app";
import 'firebase/functions'


// const removeFile = ({commit, state}, payload) => {
// 	return new Promise((resolve, reject) => {

// 	})
// };


const getAccounts = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		let addMessage = firebase.functions()
		.httpsCallable("listAllUsers");
		addMessage()
		.then(res =>{
			commit('setAccounts', res.data)
			resolve(true)
		})
		.catch(error =>{
			console.log(error);
			reject()
		});
	});
};


const getAccountInfo = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		let addMessage = firebase.functions()
		.httpsCallable("getAccountInfo");
		addMessage({id: payload.id})
		.then(res =>{
			resolve(res.data);
		})
		.catch(error =>{
			console.log(error);
			reject()

		});
	});
};

const updateAccount = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		let addMessage = firebase.functions()
		.httpsCallable("updateAccount");
		addMessage(payload)
		.then(res =>{
			resolve(res.data);
		})
		.catch(error =>{
			reject()
		});
	});
};

const createAccount = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		let addMessage = firebase.functions()
		.httpsCallable("createAccount");
		addMessage(payload)
		.then(res =>{
			if(res) {
			resolve(true);
			} else {
				throw 'no created';
			}
		})
		.catch(error =>{
			reject(error)
		});

	});
};

const removeAccount = ({commit, state}, payload) => {
	console.log(payload)
	return new Promise((resolve, reject) => {
		let addMessage = firebase.functions()
		.httpsCallable("removeAccount");
		addMessage({id: payload.id})
		.then(res =>{
			resolve(res.data);
		})
		.catch(error =>{
			reject(error)
		});
	});
};







    


     export default {
     	getAccounts,
     	getAccountInfo,
     	updateAccount,
     	removeAccount,
     	createAccount
     };
