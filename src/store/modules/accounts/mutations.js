import firebase from "firebase/app";
import 'firebase/functions'

const setAccounts = (state, payload) => {
	state.accounts = payload	
}

const setUserRole = (state, payload) => {
	return new Promise((resolve, reject) => {
		var addMessage = firebase.functions().httpsCallable("setUserRole");
		var data = { uid: payload.id, admin: payload.admin};
		addMessage(data)
		.then(res => {
			resolve(true);
		})
		.catch(err => {
			reject(err)
		});
	});
}

export default {
	setAccounts,
	setUserRole
};
