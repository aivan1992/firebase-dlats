
const getDepartmentLogs = state => {
	return state.departmentLogs;
}

const getFileName = state => {
	return state.fileName;
}



const getAllLogs = state => {
	return state.allLogs;
}


export default {
	getDepartmentLogs,
	getFileName,
	getAllLogs
};

