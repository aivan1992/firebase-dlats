import Axios from "axios";
import Vue from "vue";

const fb = require('@/firebaseConfig.js')


const getAllLogs = ({commit}, payload) => {
	commit('setAllLogs', [])
	return new Promise((resolve, reject) => {
		
		const getAllLogs = async() =>{
			let snapshot = await fb.db.collection("logs")
			.orderBy('created_at', 'desc')
			.get();
			
			let result = [];
			snapshot.docs.forEach(doc => {
				console.log(doc.metadata.creationTime);
				result.push({ id: doc.id, ...doc.data() });
			});
			return await result;
		}
		getAllLogs().then(res => {
			commit('setAllLogs', res)
			resolve(true)
		})
	})

};




const getDepartmentLogs = ({commit}, payload) => {
	commit('setDepartmentLogs', [])

	return new Promise((resolve, reject) => {
		const getDepartmentLogs = async() =>{
			let snapshot = await fb.db.collection("logs")
			.orderBy('created_at', 'desc')
			.get();
			// .where(['receiver', 'sender'], "in", payload.id)
			// return snapshot.docs.map(doc => {
			// 	return { id: doc.id, ...doc.data() }
			// });
			let result = [];
			snapshot.docs.forEach(doc => {
				if(doc.data().sender == payload.id || doc.data().receiver == payload.id) {
					result.push({ id: doc.id, ...doc.data() });
				}
			});
			return await result;
		}
		getDepartmentLogs().then(res => {
			commit('setDepartmentLogs', res)
			resolve(true)
		})
	})
};


const sendFile = ({commit , state}, payload) => {
	return new Promise((resolve, reject) => {
		let deptName = localStorage.getItem("department_name"); 

		let message = `"${payload.name}" sent "${payload.fileName}" from "${deptName}" to department "${payload.department_name}"`;

		fb.db.collection('notifications').add({
			file_name: payload.fileName,
			message,
			receiver: payload.receiver,
			sender: payload.sender,
		});

		let qr = `departments/${payload.sender}/${payload.fileName}`;

		fb.db.collection("logs").add({
			message,
			receiver: payload.receiver,
			sender: payload.sender,
			qr,
			activity: 'SEND',
			url: payload.url,
			file_name: payload.fileName,
			file_path: payload.filePath,
			created_at: moment().format('MMMM Do YYYY, h:mm:ss a')
		})
		.then(docRef => {
			resolve(true)
		})
		.catch(error => {
			reject(error);
		});
	})
};

export default {
	getDepartmentLogs,
	sendFile,
	getAllLogs
};
