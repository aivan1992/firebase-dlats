const setDepartmentLogs = (state, payload) => {
	state.departmentLogs = payload	
}

 const setFileName = (state, payload) => {
	 state.fileName = payload
}

const setAllLogs = (state, payload) => {
 state.allLogs = payload
}

export default {
	setDepartmentLogs,
	setFileName,
	setAllLogs
};
