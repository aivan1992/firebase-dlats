import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {
	departmentLogs: [],
	fileName: '',
	allLogs: []
};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
