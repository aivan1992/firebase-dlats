
const getBreadcrumbs = state => {
	return state.breadcrumbs;
}

const getNotifications = state => {
	return state.notifications;
}

const getNotificationsCount = state => {
	return state.notificationsCount;
}

export default {
	getBreadcrumbs,
	getNotifications,
	getNotificationsCount
};

