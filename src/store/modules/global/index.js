import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {
	breadcrumbs: null,
	notifications: [],
	notificationsCount: null
};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
