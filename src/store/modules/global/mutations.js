const setBreadcrumbs = (state, payload) => {
	state.breadcrumbs = payload;
}

const setNotifications = (state, payload) => {
	 state.notifications = payload;
}
const setNotificationsCount = (state, payload) => {
	 state.notificationsCount = payload;
}


export default {
	setBreadcrumbs,
	setNotifications,
	setNotificationsCount
};
