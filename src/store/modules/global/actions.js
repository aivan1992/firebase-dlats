import Axios from "axios";
import Vue from "vue";

const fb = require('@/firebaseConfig.js')



const getNotifications = ({commit}, payload) => {
	commit('setNotifications', [])
	let idNumber = localStorage.getItem("id_number"); 
	return new Promise((resolve, reject) => {

		const getNoti = async() =>{
			const snapshot = await fb.db.collection('notifications')
			.get();
			let result = [];
			snapshot.docs.forEach(doc => {
				if(doc.data().receiver == idNumber) {
					result.push({ id: doc.id, ...doc.data() });
				}
			});
			return await result;
		}
		getNoti().then(res => {
			commit('setNotifications', res)
			resolve(true)
		})
	})
};

const getNotificationsCount = ({commit}, payload) => {
	commit('setNotifications', [])
	let idNumber = localStorage.getItem("id_number"); 

	return new Promise((resolve, reject) => {
		const getNotiCount = async() =>{
			const snapshot = await fb.db.collection('notifications')
			.get();
			let result = [];
			snapshot.docs.forEach(doc => {
				if(doc.data().receiver == idNumber) {
					result.push(doc.data());
				}
			});
			return await result.length;
		}
		getNotiCount().then(res => {
			commit('setNotificationsCount', res)
			resolve(true)
		})
	})
};


const removeNoti = ({}, payload) => {
	return new Promise((resolve, reject) => {
		let ref = fb.db.collection("notifications").doc(payload.id);
		ref.delete().then(() => {
			resolve(true)
		}).catch(error=> {
			reject(error)
		});
	})
};


const downloadFile = ({}, payload) => {
	return new Promise((resolve, reject) => {
		let ref = fb.storage.ref(payload.qr);
		ref.getDownloadURL()
		.then(url => {

			let deptName = localStorage.getItem("department_name");
			let deptIdNumber = localStorage.getItem("id_number");
			let staffname = localStorage.getItem("name"); 
			let message = `${staffname} from department "${deptName}", Dept ID number "${deptIdNumber}" scanned a file with QR value of "${payload.qr}".` ;  

			fb.db.collection("logs").add({
				message,
				staff: deptIdNumber,
				qr: payload.qr,
				created_at: moment().format("DD/MM/YYYY"),
				activity: 'SCAN'
			});

			resolve(url);
		})
		.catch(err => {
			reject(err)
		})
	})
};


const downloadFileAdmin = ({}, payload) => {
	return new Promise((resolve, reject) => {
		let ref = fb.storage.ref(payload.qr);
		ref.getDownloadURL()
		.then(url => {

			resolve(url);
		})
		.catch(err => {
			reject(err)
		})
	})
};

const scanNotification = ({}, payload) => {
	return new Promise((resolve, reject) => {

		let deptName = localStorage.getItem("department_name");
		let deptIdNumber = localStorage.getItem("id_number");
		let staffname = localStorage.getItem("name"); 

		let message = `${staffname} from department "${deptName}", Dept ID number "${deptIdNumber}" scanned a file.` ;  

		let qrParse = JSON.parse(payload.qr)

		try {
			fb.db.collection("logs").add({
				message,
				staff: deptIdNumber,
				qr: payload.qr,
				url: payload.url,
				created_at: moment().format('MMMM Do YYYY, h:mm:ss a'),
				activity: 'SCAN',
				file_name: qrParse.file_name
			});
			resolve(true)
		} catch(e) {
			reject(e.message);
		}
		
	})
};

export default {
	downloadFile,
	getNotifications,
	getNotificationsCount,
	removeNoti,
	downloadFileAdmin,
	scanNotification
};
