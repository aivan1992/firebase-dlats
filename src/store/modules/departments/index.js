import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {
	departments: null,
	progressBarPercent: 0,
	files: null,
	updateFileRequiredFields: null,
};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
