const setDepartments = (state, payload) => {
	state.departments = payload	
}

const setProgressBarPercent = (state, payload) => {
	state.progressBarPercent = payload	
}

const setFiles = (state, payload) => {
	state.files = payload
}

const setUpdateFileRequiredFields = (state, payload) => {
	state.updateFileRequiredFields = payload;
}

export default {
	setDepartments,
	setProgressBarPercent,
	setFiles,
	setUpdateFileRequiredFields
};
