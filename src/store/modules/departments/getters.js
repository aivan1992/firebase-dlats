
const getDepartments = (state) => {
	return state.departments;
}

const getProgressBarPercent = (state) => {
	return state.progressBarPercent;
}

const getFiles = (state) => {
	return state.files;
}
const getUpdateFileRequiredFields = state => {
	return state.updateFileRequiredFields;
}

export default {
	getDepartments,
	getProgressBarPercent,
	getFiles,
	getUpdateFileRequiredFields
};
