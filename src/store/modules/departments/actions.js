import Axios from "axios";
import Vue from "vue";

const fb = require('@/firebaseConfig.js')

const createDepartment = ({commit , state}, payload) => {
	return new Promise((resolve, reject) => {
		var departments = fb.db.collection("departments");
		var query = departments.where("id_number", "==", payload.id_number);
		query.get().then(snapshot => {
			if(snapshot.empty) {
				departments.add(payload)
				.then(res =>  {
					resolve(true)
				})
				.catch(err => {
					vm.swalErrorMessage();
					resolve(false)
					
				})
			} else {
				vm.swalErrorMessage('Error, ID number exist')
				resolve(false)
				
			}
		})
	})
};

const updateDepartment = ({commit , state}, payload) => {
	return new Promise((resolve, reject) => {
		var departments = fb.db.collection("departments");
		departments.doc(payload.id).update({
			name: payload.form.name,
			timestamp: fb.firebase.firestore.FieldValue.serverTimestamp()
		}).then(res => {
			resolve(true)
		})
		.catch(err => {
			vm.swalErrorMessage();
			resolve(false)
		});

	})
};

const getDepartments = ({commit}, payload) => {
	commit('setDepartments', [])

	return new Promise((resolve, reject) => {

		const getDepartments = async() =>{
			const snapshot = await fb.db.collection('departments')
			.orderBy('name')
			// .limit(10)
			.get();

			return snapshot.docs.map(doc => {
				return { id: doc.id, ...doc.data() }
			});
		}

		getDepartments().then(res => {
			commit('setDepartments', res)
			resolve(true)
		})
	})
};

const getDepartmentInfo = ({commit}, payload) => {
	return new Promise((resolve, reject) => {
		const result = fb.db.collection('departments')
		.where(fb.firebase.firestore.FieldPath.documentId(), '==', payload.id)
		.get();

		result
		.then(snapshot => {
			resolve(snapshot.docs[0].data())
		})
		.catch(err => {
			reject()
		})


	})
};

const checkFileExist = ({}, payload) => {
	return new Promise((resolve, reject) => {
		const storageRef= fb.storage.ref(`departments/${payload.id_number}`);
		storageRef.child(`${payload.file_name}`).getDownloadURL().then(onResolve, onReject);
		function onResolve(foundURL) {
			resolve(true);
		}
		function onReject(error) {
			resolve(false);
		}
	})
};

const addFile = ({commit,state}, payload) => {
	return new Promise((resolve, reject) => {

		state.progressBarPercent = 0;
		
		const storageRef = fb.storage
		.ref(`departments/${payload.id_number}/${payload.file_name}`)
		.put(payload.file);

		storageRef.on(`state_changed`, 
			snapshot => {
				state.progressBarPercent = (snapshot.bytesTransferred/snapshot.totalBytes)*100;
			}, 
			error => {
				console.log(error.message)
			},
			() => {
				state.progressBarPercent = 100;
				resolve(true);
				// storageRef.snapshot.ref.getDownloadURL().then(url=> {
				// });
			})  
	})
};

const viewFiles = ({commit,state}, payload) => {
	commit('setFiles', []);
	return new Promise((resolve, reject) => {
		const storageRef= fb.storage.ref(`departments`);
		var listRef = storageRef.child(`${payload.id_number}`);

		listRef.listAll().then(res => {
			let items = [];

			let queue = res.items.map(async (itemRef) => {
				let item = {};
				await itemRef.getDownloadURL().then(url => {
					item['url'] = url;
				});

				await itemRef.getMetadata().then(function(metadata) {
					item['type'] = metadata.contentType;
					item['size'] = metadata.size;
					item['created_at'] = metadata.timeCreated;
					item['name'] = metadata.name;
				})
				return items.push(item);
			});	

			Promise.all(queue).then(res => {
				commit('setFiles', items);
				resolve(true);
			})

		}).catch(error => {
			reject(error)
		});
	})
};

const removeFile = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		const storageRef= fb.storage.ref(`departments`);
		var desertRef = storageRef.child(`${payload.id_number}/${payload.name}`);
		desertRef.delete()
		.then(() => {
			resolve(true);
		})
		.catch(error => {
			reject(error)
		});
	})
};

const updateFile = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {

		state.progressBarPercent = 0;
		
		const storageRef = fb.storage
		.ref(`departments/${payload.id_number}/${payload.file_name}`)
		.put(payload.file);

		storageRef.on(`state_changed`, 
			snapshot => {
				state.progressBarPercent = (snapshot.bytesTransferred/snapshot.totalBytes)*100;
			}, 
			error => {
				console.log(error.message)
			},
			() => {
				state.progressBarPercent = 100;
				storageRef.snapshot.ref.getDownloadURL().then(url=> {
					resolve(true);
				});
			})  
	})
};


const removeDepartment = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {
		fb.db.collection('departments').doc(payload.id).delete()
		.then(() => {
			const storageRef= fb.storage.ref(`departments`);
			var listRef = storageRef.child(`${payload.id_number}`);

			listRef.listAll()
			.then(async res => {
				await res.items.map(async itemRef => {
					return itemRef.getMetadata().then(async metadata => {
						console.log(metadata.name)
						return listRef.child(metadata.name).delete();
					})
				});
				return await resolve(true);
			})
			.catch(err => {
				reject(err);
			});
		});
	});
};


const acceptFile = ({commit, state}, payload) => {
	return new Promise((resolve, reject) => {


		let  createFile = async() => {
			let response = await fetch(payload.url, {mode: 'no-cors'});
			let data = await response.blob();
			
			return new File([data], payload.file_name);
		}

		createFile().then(res => {
			var file = res;
		
		// var file = new File("payload.url");
		state.progressBarPercent = 0;

		const storageRef = fb.storage
		.ref(`departments/${payload.id_number}/${payload.file_name}`)
		.put(file);

		storageRef.on(`state_changed`, 
			snapshot => {
				state.progressBarPercent = (snapshot.bytesTransferred/snapshot.totalBytes)*100;
			}, 
			error => {
				console.log(error.message)
			},
			() => {
				state.progressBarPercent = 100;
				resolve(true);
			}) 

		})
	})
};


export default {
	checkFileExist,
	createDepartment,
	getDepartments,
	getDepartmentInfo,
	addFile,
	viewFiles,
	removeFile,
	updateFile,
	updateDepartment,
	removeDepartment,
	acceptFile
};
