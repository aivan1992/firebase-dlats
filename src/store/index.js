import Vue from 'vue'

import Vuex from 'vuex'
Vue.use(Vuex)

import auth from '@/modules/auth/store'
import departments from './modules/departments'
import accounts from './modules/accounts'
import global from './modules/global'
import logs from './modules/logs'


const store = new Vuex.Store({
	modules: {
		auth,
		departments,
		accounts,
		global,
		logs
	},
	state: {},
	mutations: {},
	actions: {}

})


export default store;