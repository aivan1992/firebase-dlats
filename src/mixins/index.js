
export const mixin = {

	data() {

		return {
		}

	},

	methods: {

		toTitleCase(str) {
			return str.replace(

				/\w\S*/g,
				function(txt) {
					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
				}

				);

		},

		removeNotNumber(val) {

			return val = val.toString().replace(/[\$\,a-zA-Z\s]/g,'')
		},

		formatAsThousands(val) {

			val = (+(val)).toFixed(2); 

			return val = val.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

		},

		toast(body, variant = 'success') {
			return new Promise((resolve, reject) => {
				this.$bvToast.toast(body, {
					title: ``,
					toaster: 'b-toaster-bottom-right',
					solid: true,
					appendToast: true,
					variant: variant,
				});

				setTimeout(() => {
					resolve(true);
				}, 2000);

			});
		},

		swal(text, type = 'success') {
			return new Promise((resolve, reject) => {
				Swal.fire({
					title:  'Are you sure?',
					text: text,
					icon: 'warning',
					confirmButtonText: 'Proceed',
					showCancelButton: true,
					confirmButtonColor: 'gray'
				}).then((result) => {
					if(result.value) {
						resolve(true);
					} else {
						resolve(false);
					}
				});
			});
		},

		swalRemove(text) {
			return new Promise((resolve, reject) => {
				Swal.fire({
					title:  'Are you sure?',
					text: 'You wont able to revert this.',
					icon: 'warning',
					confirmButtonText: 'Proceed',
					showCancelButton: true,
					confirmButtonColor: 'gray'
				}).then((result) => {
					if(result.value) {
						resolve(true);
					} else {
						resolve(false);
					}
				})
			})
		},

		swalWarningMessage(text) {
			Swal.fire({
				icon: 'warning',
				title: text || 'Input fields invalid',
				showConfirmButton: false,
				timer: 2000
			})
		},

		swalSuccessMessage(text) {
			Swal.fire({
				icon: 'success',
				title: text || 'The data has been saved',
				showConfirmButton: false,
				timer: 2000
			})
		},

		swalErrorMessage(text) {
			Swal.fire({
				icon: 'error',
				title: text || 'Error occured',
				showConfirmButton: false,
				timer: 2000
			})
		},

		getStaffRoute() {
			let dept = localStorage.getItem("department"); 
			if (dept !== null) {
				return `/departments/view-files/${dept}`
			}
			return '/';
		},

		downloadFile(qr) {
			this.$store.dispatch('global/downloadFile', {qr})
			.then(res => {
				alert(res);
				window.open(res);
			})
			.catch(err => {
				vm.swalErrorMessage();
			})
		}

	}
}

